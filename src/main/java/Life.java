/**
 * Created with IntelliJ IDEA.
 * User: Remco
 * Date: 28/10/13
 * Time: 16:10
 * To change this template use File | Settings | File Templates.
 */

import com.jme3.app.SimpleApplication;
import com.jme3.collision.CollisionResults;
import com.jme3.font.BitmapText;
import com.jme3.input.KeyInput;
import com.jme3.input.MouseInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.AnalogListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.input.controls.MouseButtonTrigger;
import com.jme3.math.Ray;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.scene.Node;
import net.java.games.input.Keyboard;

public class Life extends SimpleApplication {

    GridCanvas gridCanvas;
    Cell previousCell;
    boolean mousePressed;
    BitmapText gridText;

    public static void main(String[] args) {
       Life app = new Life();
        app.start();
    }

    @Override
    public void simpleInitApp() {
        //jmonkey boilercode
        initKeys();
        initCrossHairs(); // a "+" in the middle of the screen to help aiming
        cam.setLocation(new Vector3f(-7, 9.5f, 3.5f));
        flyCam.setMoveSpeed(10f);

        //Create grid and grid manager objects
        CellGrid grid = new PlaneGrid(20, 20);
        gridCanvas = new GridCanvas(grid, 0, 0);
        rootNode.attachChild(gridCanvas.getRenderable(assetManager));


        guiFont = assetManager.loadFont("Interface/Fonts/Default.fnt");
        gridText = new BitmapText(guiFont, false);
        gridText.setSize(guiFont.getCharSet().getRenderedSize());
        gridText.setText("Plane");
        gridText.setLocalTranslation(300, gridText.getLineHeight(), 0);
        guiNode.attachChild(gridText);
    }

    @Override
    public void simpleUpdate(float tpf) {
        // light source goes with cam
        //spot.setPosition(cam.getLocation());               // shine from camera loc
        //spot.setDirection(cam.getDirection());

        //if(cam.getLocation().subtract(lastPlayerLocation.) > 50)
    }

    @Override
    public void simpleRender(RenderManager rm) {
    }

    public void initKeys() {
        inputManager.addMapping("click", new KeyTrigger(KeyInput.KEY_SPACE), // trigger 1: spacebar
                new MouseButtonTrigger(MouseInput.BUTTON_LEFT)); // trigger 2: left-button click
        inputManager.addListener(analogListener, "click");

        inputManager.addMapping("nextGen", new KeyTrigger(KeyInput.KEY_2)); // trigger 2: left-button click
        inputManager.addListener(actionListener, "nextGen");

        inputManager.addMapping("prevGen", new KeyTrigger(KeyInput.KEY_1)); // trigger 2: left-button click
        inputManager.addListener(actionListener, "prevGen");

        inputManager.addMapping("reset", new KeyTrigger(KeyInput.KEY_E)); // trigger 2: left-button click
        inputManager.addListener(actionListener, "reset");

        inputManager.addMapping("plane", new KeyTrigger(KeyInput.KEY_8)); // trigger 2: left-button click
        inputManager.addListener(actionListener, "plane");

        inputManager.addMapping("sphere", new KeyTrigger(KeyInput.KEY_9)); // trigger 2: left-button click
        inputManager.addListener(actionListener, "sphere");

        inputManager.addMapping("mobius", new KeyTrigger(KeyInput.KEY_0)); // trigger 2: left-button click
        inputManager.addListener(actionListener, "mobius");
  }

    protected void setGridString(String text)
    {
        gridText.setText(text);
    }

    /** A centred plus sign to help the player aim. */
    protected void initCrossHairs() {
        setDisplayStatView(false);
        guiFont = assetManager.loadFont("Interface/Fonts/Default.fnt");
        BitmapText ch = new BitmapText(guiFont, false);
        ch.setSize(guiFont.getCharSet().getRenderedSize() * 2);
        ch.setText("+"); // crosshairs
        ch.setLocalTranslation( // center
                settings.getWidth() / 2 - ch.getLineWidth()/2, settings.getHeight() / 2 + ch.getLineHeight()/2, 0);
        guiNode.attachChild(ch);
    }


    //defining the left mouse button click event
  private ActionListener actionListener = new ActionListener() {
    public void onAction(String name, boolean keyPressed, float tpf) {
       if (name.equals("nextGen") && keyPressed == true)
       {
           gridCanvas.goNextGeneration();
           rootNode.detachAllChildren();
           rootNode.attachChild(gridCanvas.getRenderable(assetManager));
       }
        if (name.equals("prevGen") && keyPressed == true)
        {
            gridCanvas.goPreviousGeneration();
            rootNode.detachAllChildren();
            rootNode.attachChild(gridCanvas.getRenderable(assetManager));
        }
        if (name.equals("reset") && keyPressed == true)
        {
            gridCanvas.reset(new PlaneGrid(20, 20));
            rootNode.detachAllChildren();
            rootNode.attachChild(gridCanvas.getRenderable(assetManager));
        }

        if (name.equals("plane") && keyPressed == true)
        {
            gridCanvas.reset(new PlaneGrid(20, 20));
            setGridString("Plane");
            rootNode.detachAllChildren();
            rootNode.attachChild(gridCanvas.getRenderable(assetManager));
        }
        if (name.equals("sphere") && keyPressed == true)
        {
            gridCanvas.reset(new SphereGrid(20, 20));
            setGridString("Sphere");
            rootNode.detachAllChildren();
            rootNode.attachChild(gridCanvas.getRenderable(assetManager));
        }
        if (name.equals("mobius") && keyPressed == true)
        {
            gridCanvas.reset(new MobiusGrid(20, 20));
            setGridString("Mobius");
            rootNode.detachAllChildren();

            rootNode.attachChild(gridCanvas.getRenderable(assetManager));
        }
    }
    };
    private AnalogListener analogListener = new AnalogListener() {
    public void onAnalog(String name, float value, float tpf) {
        if (name.equals("click")) {
            Ray ray = new Ray(cam.getLocation(), cam.getDirection());
            Integer col = ray.collideWith(gridCanvas.getBoundingBox().getBound(), new CollisionResults());
            //if(col != 0)
            //{
                Cell newCell =  gridCanvas.getCell(ray);
                if((previousCell == null || newCell != previousCell) && newCell != null)
                {
                    previousCell = newCell;

                    newCell.toggle();
                    rootNode.detachAllChildren();
                    rootNode.attachChild(gridCanvas.getRenderable(assetManager));
                }
 //           }
        }
    }
    };
}
