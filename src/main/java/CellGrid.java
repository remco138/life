import com.jme3.asset.AssetManager;
import com.jme3.scene.Node;
import java.awt.*;

//Inheriting classes should be final
//because the design allows immutable classes
public abstract class CellGrid
{
    protected Cell[][] grid;
    protected int rows, cols;
    protected Shape shapes;

    protected abstract Cell[][] getGrid();
    protected abstract int getRows();
    protected abstract int getCols();

    protected abstract CellGrid getNextGeneration();
    protected abstract int checkWrappingNeighbour(int x, int y);

    protected CellGrid(int rows, int cols)
    {
        this.cols = cols;
        this.rows = rows;

        grid = new Cell[rows][cols];
        for (int y = 0; y < cols; y++)
        {
            for (int x = 0; x < rows; x++)
            {
                grid[x][y] = new Cell(0);
                //System.out.println(Integer.toString(x) + " - " + Integer.toString(y));
                //System.out.println(grid[x][y].currentState.toString());
            }
        }
    }

    protected CellGrid(Cell[][] cells)
    {
        rows = cells.length;
        cols = cells[0].length;

        grid = cells;
    }



    public Cell getCell(int x, int y)
    {
        return grid[x][y];
    }
    public boolean toggle(int x, int y)
    {
        if(x > rows || y > cols)
            return false;

        //If dead, "return" alive, else dead. java's enums are stupid so a = !a might or might not work.
        grid[x][y].currentState = (grid[x][y].currentState == 0) ? 1 : 0;
        return true;
    }



    public int checkNeighbours(int x, int y)
    {
        int neighbours = 0;
        Point possibleCoords[] = new Point[8];

        possibleCoords[0] = new Point(x - 1 , y - 1);
        possibleCoords[1] = new Point(x , y - 1);
        possibleCoords[2] = new Point(x + 1 , y - 1);
        possibleCoords[3] = new Point(x - 1 , y);
        possibleCoords[4] = new Point(x + 1 , y);
        possibleCoords[5] = new Point(x - 1 , y + 1);
        possibleCoords[6] = new Point(x , y + 1);
        possibleCoords[7] = new Point(x + 1 , y + 1);

        for (int i = 0; i < possibleCoords.length; i++)
        {
            int px = possibleCoords[i].x;
            int py = possibleCoords[i].y;

            try
            {
                if (grid[px][py].currentState > 0)
                {
                    neighbours += 1;
                }
            }
            catch (ArrayIndexOutOfBoundsException e)
            {
                neighbours += checkWrappingNeighbour(px, py);
            }

        }

        return neighbours;
    }






    //abstract void initialize_Grid(int col, int row);

    //protected abstract Node getRenderable(AssetManager assetManager);
    //protected abstract Node renderCells(AssetManager assetManager);
    //protected abstract Node renderGrid(AssetManager assetManager);
}
/*
public class CellGrid
{
    public Cell grid[][];
    private int col , row;
    private int cellSize;


    public CellGrid(int col, int row, int cellSize)
    {
        this.col = col;
        this.row = row;
        this.cellSize = cellSize;
        initialize_Grid(col, row);
    }

    private void initialize_Grid(int col, int row)
    {
        grid = new Cell[row][col];
        for (int y = 0; y < col; y++)
        {
            for (int x = 0; x < row; x++)
            {
                grid[x][y] = new Cell(Cell.State.EMPTY);
                System.out.println(Integer.toString(x) + " - " + Integer.toString(y));
                System.out.println(grid[x][y].currentState.toString());
            }
        }
    }



    //debug
    private String return_State(int x, int y)
    {
        return grid[x][y].currentState.toString();
    }
}
 */