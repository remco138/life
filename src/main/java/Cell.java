/**
 * Created with IntelliJ IDEA.
 * User: Remco
 * Date: 28/10/13
 * Time: 17:42
 * To change this template use File | Settings | File Templates.
 */

public class Cell
{
    int currentState;

    public enum State
    {
        DEAD,
        ALIVE,
        EMPTY //?
    }

    public Cell(int state)
    {
        currentState = state;
    }

    public void toggle()
    {
        //currentState = (currentState == Cell.State.DEAD) ? State.ALIVE : State.DEAD;
        currentState = (currentState == 0) ? 1 : 0;
    }

    public void set(int state)
    {
        currentState = state;
    }
}