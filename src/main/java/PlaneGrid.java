import javafx.util.Pair;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Remco
 * Date: 29/10/13
 * Time: 15:47
 * To change this template use File | Settings | File Templates.
 */



public class PlaneGrid extends CellGrid {
    //public Cell grid[][];
    //private int cols, rows;
    public PlaneGrid(int rows, int cols)
    {
        super(rows, cols);
    }

   // public PlaneGrid(Cell[][] cells)
  // {
  // }


    public Cell[][] getGrid()
    {
        return grid;
    }
    public int getRows()
    {
        return rows;
    }
    public int getCols()
    {
        return cols;
    }

////////////////////////////////////////////////////////////////////////////////////////o
    //Receives a point (cell location) which could be wrapped depending on the surface
    //CellGrid that is being used
    @Override
    protected int checkWrappingNeighbour(int x, int y)
    {
        //PlaneGrid doesn't wrap to other edges
        if(x < 0 || x > rows ||
           y < 0 || y > cols)
            return 0;

        return 0; //Shouldn't reach this..

       /*
        if (x < 0)
        {
            possibleCoords[0] = null;
            possibleCoords[1] = null;
            possibleCoords[2] = null;
        }
        if(x + 1 > cols)
        {
            possibleCoords[5] = null;
            possibleCoords[6] = null;
            possibleCoords[7] = null;
        }
        if (y - 1 < 0)
        {
            possibleCoords[0] = null;
            possibleCoords[3] = null;
            possibleCoords[5] = null;
        }
        if(y + 1 > rows)
        {
            possibleCoords[2] = null;
            possibleCoords[4] = null;
            possibleCoords[7] = null;
        }*/
    }

     protected CellGrid getNextGeneration()
    {
        CellGrid newGrid = new PlaneGrid(rows, cols);

        for (int y = 0; y < rows; y++)
        {
            for (int x = 0;x < cols; x++)
            {
                System.out.println(Integer.toString(x) + " - " + Integer.toString(y));
                if (grid[x][y].currentState > 0)
                {
                    if(checkNeighbours(x, y) < 2)
                    {
                        newGrid.grid[x][y].currentState = 0;
                    }
                    if(checkNeighbours(x, y) == 2 || checkNeighbours(x, y) == 3)
                    {
                        newGrid.grid[x][y].currentState = grid[x][y].currentState+1;
                    }
                    if(checkNeighbours(x, y) > 3)
                    {
                        newGrid.grid[x][y].currentState = 0;
                    }

                }
                else if (grid[x][y].currentState == 0)
                {
                    if(checkNeighbours(x, y) == 3)
                    {
                        newGrid.grid[x][y].currentState = 1; //same as in setting to 1, feel free to change it to that
                    }
                }
            }
        }

        return newGrid;
    }



    //debug
    private String return_State(int x, int y)
    {
        //return grid[x][y].currentState.toString();
        return "yo";
    }
}
