/**
 * Created with IntelliJ IDEA.
 * User: Remco
 * Date: 29/10/13
 * Time: 16:28
 * To change this template use File | Settings | File Templates.
 */
import com.jme3.asset.AssetManager;
import com.jme3.collision.CollisionResult;
import com.jme3.collision.CollisionResults;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Ray;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Mesh;
import com.jme3.material.Material;
import com.jme3.scene.shape.Box;
import com.jme3.scene.shape.Line;

import java.util.*;

public class GridCanvas {
    private Node gridLines;
    private Node gridCells;
    private Box boundingBox;

    private CellGrid grid;
    private CellGrid initialGrid;
    private float cellSize;
    private float lineWidth;
    private float  x, y;
    private int generation;

    private List<CellGrid> history;


    public GridCanvas (CellGrid grid, float x, float y)
    {
        this.grid = grid;
        initialGrid = grid;
        this.x = x;
        this.y = y;
        this.cellSize = 1f;
        this.lineWidth = 5f;
        this.history = new ArrayList<CellGrid>();
        boundingBox = new Box(grid.rows*cellSize, 0f, grid.rows*cellSize);

        history.add(grid);
    }

    public void setCellSize(float size)
    {
       this.cellSize = size;
    }
    public void setLineWidth(float width)
    {
        this.lineWidth = width;
    }

    public Box getBoundingBox()
    {
        return boundingBox;
    }

    public boolean toggleCell(Ray viewRay)
    {
        CollisionResults results = new CollisionResults();
        CollisionResult result = new CollisionResult();
        gridCells.collideWith(viewRay, results);
        result = results.getClosestCollision();
        if(result == null)
            return false;

        for(int y = 0; y < grid.cols; y++)
        {
            for(int x = 0; x < grid.cols; x++)
            {
                //Not very pretty however it should work
                if(result.getGeometry().equals(gridCells.getChild(y * grid.rows + x)))
                {
                    grid.toggle(x, y);
                    return true;
                }
            }
        }
        return false;
   }

    public boolean toggleCell(int x, int y)
    {
        return grid.toggle(x, y);
    }

    public void goNextGeneration()
    {
        setGeneration(generation+1);
    }

    public void goPreviousGeneration()
    {
        setGeneration(generation-1);
    }

    public void setGeneration(int newGen)
    {
        //Some ternary operator magic might've been used here
        //however for readability reasons we decided to go for if/else statements
        if(newGen < 0)
        {
            return;
        }
        if(newGen > generation)
        {
            for(int i = 0; i < newGen - generation; i++)
            {
                grid = grid.getNextGeneration();
                if(history.size() <= newGen)
                {
                    history.add(grid);
                }
            }
        }
        else if(newGen < generation)
        {
            //for(int i = 0; i > newGen - generation; i--)
            //{
            //    grid = grid.getNextGeneration();
            //}
            grid = history.get(newGen);

        }
        generation = newGen;
        //nothing changes when newGen is the same as the previous value
    }

    public void reset(CellGrid state)
    {
        generation = 0;
        history.clear();
        history.add(state);
        grid = state;
    }

    public Cell getCell(Ray ray)
    {
        CollisionResults results = new CollisionResults();
        CollisionResult result = new CollisionResult();
        gridCells.collideWith(ray, results);
        result = results.getClosestCollision();
        if(result != null)
        {
            for(int y = 0; y < grid.cols; y++)
            {
                for(int x = 0; x < grid.cols; x++)
                {
                    //Not very pretty however it should work
                    if(result.getGeometry().equals(gridCells.getChild(y * grid.rows + x)))
                    {
                        return grid.getCell(x, y);
                    }
                }
            }
        }

        return null;
    }

    public Node getRenderable(AssetManager assetManager)
    {
        Node node = new Node();
        node.attachChild(renderCells(assetManager));
        node.attachChild(renderGrid(assetManager));

        return node;
    }

    private Node renderCells(AssetManager assetManager)
    {
        //todo: make stuff class-global to prevent recreating node hierarchy every cell toggle
        Node node = new Node();

        for (int y = 0; y < grid.cols; y++)
        {
            for (int x = 0; x < grid.rows; x++)
            {
                ColorRGBA color;
                if(grid.getCell(x, y).currentState == 0)
                    color = ColorRGBA.Blue;
                else
                {
                    float colorVal =  grid.getCell(x, y).currentState / 5f;
                color = new ColorRGBA(colorVal, 0.5f, 0, 0);
                }
                //(grid.getCell(x, y).currentState > 0) ? ColorRGBA.Red : ColorRGBA.Blue;


                float boxSize = cellSize/2f;
                Mesh box = new Box(boxSize, 0.5f, boxSize);//(e.x, 0f, e.y);
                Material mat1 = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
                //Vector3f translate = new Vector3f(minX, 0, minY);
                Geometry boxGeometry = new Geometry("line", box);
                boxGeometry.setLocalTranslation(new Vector3f(x*cellSize + boxSize, 0.5f, y*cellSize + boxSize));
                boxGeometry.setMaterial(mat1);
                mat1.setColor("Color", color);
                node.attachChild(boxGeometry);
            }
        }
        gridCells = node;
        return node;
    }

    private Node renderGrid(AssetManager assetManager)
    {
        Node node = new Node();

        for (int x = 0; x <= grid.rows; x++)
        {
            Vector3f start = new Vector3f(x*cellSize, 1.02f, 0f);
            Vector3f end = new Vector3f(x*cellSize, 1.02f, cellSize*grid.cols);
            Mesh line = new Line(start, end);
            line.setLineWidth(lineWidth);
            Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
            Geometry lineGeometry = new Geometry("line", line);

            mat.setColor("Color", ColorRGBA.Brown);
            lineGeometry.setMaterial(mat);
            node.attachChild(lineGeometry);
        }
        for (int y = 0; y <= grid.cols; y++)
        {
            Vector3f start = new Vector3f(0f, 1.02f, y*cellSize);
            Vector3f end = new Vector3f(cellSize*grid.rows, 1.02f, y*cellSize);
            Mesh line = new Line(start, end);
            line.setLineWidth(lineWidth);
            Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
            Geometry lineGeometry = new Geometry("line", line);

            mat.setColor("Color", ColorRGBA.Brown);
            lineGeometry.setMaterial(mat);
            node.attachChild(lineGeometry);
        }

        return node;
    }
}
